# translation of joystick.po to Kurdish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Erdal Ronahi <erdal.ronahi@nospam.gmail.com>, 2007, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: joystick\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2009-01-06 13:56+0200\n"
"Last-Translator: Erdal Ronahi <erdal.ronahi@nospam.gmail.com>\n"
"Language-Team: Kurdish <kde-i18n-doc@lists.kde.org>\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Ezadin Mahmoud"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ezadin76@gmail.com"

#: caldialog.cpp:26 joywidget.cpp:339
#, kde-format
msgid "Calibration"
msgstr "Pîvan"

#: caldialog.cpp:40
#, kde-format
msgid "Next"
msgstr "Pêşve"

#: caldialog.cpp:50
#, kde-format
msgid "Please wait a moment to calculate the precision"
msgstr "Tikaye raweste ta ku hûrî bên jimardin"

#: caldialog.cpp:79
#, kde-format
msgid "(usually X)"
msgstr "(bi asayî X)"

#: caldialog.cpp:81
#, kde-format
msgid "(usually Y)"
msgstr "(bi asayî Y)"

#: caldialog.cpp:87
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>minimum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Pîvandin warê nirxa ku amûrê dide, azmûn dike.<br /><br />Tikaye rake "
"<b>tewere %1 %2</b>ji ser amûrê xwe bibe ser <b>kêmtirîn</b> der.<br /><br /"
">Bişkokekî amûrê bi kar bîne yanjî  \" Pêşve\" bitepîne jibo bi gava li pêş "
"berdewam bike .</qt>"

#: caldialog.cpp:110
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>center</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Pîvandin warê nirxa ku amûrê dide, azmûn dike.<br /><br />Tikaye rake "
"<b>tewere %1 %2</b>ji ser amûrê xwe bibe ser <b>navend</b> der.<br /><br /"
">Bişkokekî amûrê bi kar bîne yanjî  \" Pêşve\" bitepîne jibo bi gava li pêş "
"berdewam bike .</qt>"

#: caldialog.cpp:133
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>maximum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Pîvandin warê nirxa ku amûrê dide, azmûn dike.<br /><br />Tikaye rake "
"<b>tewere %1 %2</b>ji ser amûrê xwe bibe ser <b>bihtirîn</b> der.<br /><br /"
">Bişkokekî amûrê bi kar bîne yanjî  \" Pêşve\" bitepîne jibo bi gava li pêş "
"berdewam bike .</qt>"

#: caldialog.cpp:160 joywidget.cpp:329 joywidget.cpp:365
#, kde-format
msgid "Communication Error"
msgstr "Çewtyeke ragihandinî"

#: caldialog.cpp:164
#, kde-format
msgid "You have successfully calibrated your device"
msgstr "Te amûra xwe bi serkeftin pîvand"

#: caldialog.cpp:164 joywidget.cpp:367
#, kde-format
msgid "Calibration Success"
msgstr "Pîvandin bi serket"

#: caldialog.cpp:184
#, kde-format
msgid "Value Axis %1: %2"
msgstr "Nirxê tewere %1: %2"

#: joydevice.cpp:41
#, kde-format
msgid "The given device %1 could not be opened: %2"
msgstr "Ev Amûr %1 nehat vekirin: %2"

#: joydevice.cpp:45
#, kde-format
msgid "The given device %1 is not a joystick."
msgstr "Ev Amûr %1 ne joystick e."

#: joydevice.cpp:49
#, kde-format
msgid "Could not get kernel driver version for joystick device %1: %2"
msgstr "Guhertoya ajokarê kernel jibo amûrê joystick peyîde nebû %1: %2"

#: joydevice.cpp:60
#, kde-format
msgid ""
"The current running kernel driver version (%1.%2.%3) is not the one this "
"module was compiled for (%4.%5.%6)."
msgstr ""
"Guherto yê ajokarê kernel yê niha (%1.%2.%3) ne yê ku jêre ev module  berhev "
"bûye (%4.%5.%6)."

#: joydevice.cpp:71
#, kde-format
msgid "Could not get number of buttons for joystick device %1: %2"
msgstr "Hijmara bişkokê amûrê joystick nehatin naskirin%1: %2"

#: joydevice.cpp:75
#, kde-format
msgid "Could not get number of axes for joystick device %1: %2"
msgstr "Hijmara tewere yê  amûrê joystick nehatin naskirin%1: %2"

#: joydevice.cpp:79
#, kde-format
msgid "Could not get calibration values for joystick device %1: %2"
msgstr "Nirxê pîvanê ya amûrê joystick nehatin naskirin %1: %2"

#: joydevice.cpp:83
#, kde-format
msgid "Could not restore calibration values for joystick device %1: %2"
msgstr "Nirxê pîvanê yê amûrê joystick nehatin vegerandin %1: %2"

#: joydevice.cpp:87
#, kde-format
msgid "Could not initialize calibration values for joystick device %1: %2"
msgstr "Nirxê pîvanê ya amûrê joystick nehatin destpêkirin %1: %2"

#: joydevice.cpp:91
#, kde-format
msgid "Could not apply calibration values for joystick device %1: %2"
msgstr "Nirxê pîvanê ya amûrê joystick  bikar nehatin %1: %2"

#: joydevice.cpp:95
#, kde-format
msgid "internal error - code %1 unknown"
msgstr "Çewetîyek hundirî - kod %1 nenas"

#: joywidget.cpp:67
#, kde-format
msgid "Device:"
msgstr "Amûr:"

#: joywidget.cpp:84
#, kde-format
msgctxt "Cue for deflection of the stick"
msgid "Position:"
msgstr "Cih:"

#: joywidget.cpp:87
#, kde-format
msgid "Show trace"
msgstr "Şopê diyar bike"

#: joywidget.cpp:96 joywidget.cpp:303
#, kde-format
msgid "PRESSED"
msgstr "TEPANDÎ"

#: joywidget.cpp:98
#, kde-format
msgid "Buttons:"
msgstr "Bişkok:"

#: joywidget.cpp:102
#, kde-format
msgid "State"
msgstr "Rewş"

#: joywidget.cpp:110
#, kde-format
msgid "Axes:"
msgstr "Tewere:"

#: joywidget.cpp:114
#, kde-format
msgid "Value"
msgstr "Nirx"

#: joywidget.cpp:127
#, kde-format
msgid "Calibrate"
msgstr "Pîvan"

#: joywidget.cpp:190
#, kde-format
msgid ""
"No joystick device automatically found on this computer.<br />Checks were "
"done in /dev/js[0-4] and /dev/input/js[0-4]<br />If you know that there is "
"one attached, please enter the correct device file."
msgstr ""
"Lêgerîna bixwebixwe li amûrê joystick biser neket.<br />Di  /dev/js[0-4] û "
"di /dev/input/js[0-4] hate lêgerîn <br /> Heke tu dizane ku amûrek hatîye "
"girêdan,tikaye doseya durust ya amûrê bide."

#: joywidget.cpp:226
#, kde-format
msgid ""
"The given device name is invalid (does not contain /dev).\n"
"Please select a device from the list or\n"
"enter a device file, like /dev/js0."
msgstr ""
"Navê amûrê ne rewa ye ( /dev  têde tune).\n"
"Tikaye amûrekê ji lîstê hilbijêre yan\n"
"dosya amûrekê bide, wek /dev/js0."

#: joywidget.cpp:229
#, kde-format
msgid "Unknown Device"
msgstr "Amûrek neneas"

#: joywidget.cpp:247
#, kde-format
msgid "Device Error"
msgstr "Çewtîyek amûrê"

#: joywidget.cpp:265
#, kde-format
msgid "1(x)"
msgstr "1(x)"

#: joywidget.cpp:266
#, kde-format
msgid "2(y)"
msgstr "2(y)"

#: joywidget.cpp:335
#, kde-format
msgid ""
"<qt>Calibration is about to check the precision.<br /><br /><b>Please move "
"all axes to their center position and then do not touch the joystick anymore."
"</b><br /><br />Click OK to start the calibration.</qt>"
msgstr ""
"<qt>Pîvan hûrî azmûn dike.<br /><br /><b>Tikaye hemû tewereyan bibe dera "
"navend û dest nede joystick.</b><br /><br />Jibo bidestpêkirna pîvan, Ok "
"bitepî ne .</qt>"

#: joywidget.cpp:367
#, kde-format
msgid "Restored all calibration values for joystick device %1."
msgstr "Hemû nirxê pîvanê yê amûrê joystick vegerîne %1."

#~ msgid "KDE Joystick Control Module"
#~ msgstr "KDE Joystick Control Module"

#, fuzzy
#~| msgid "KDE Control Center Module to test Joysticks"
#~ msgid "KDE System Settings Module to test Joysticks"
#~ msgstr "KDE Control Center Module, Joystickan azmûne dike"

#~ msgid "(c) 2004, Martin Koller"
#~ msgstr "(c) 2004, Martin Koller"

#~ msgid ""
#~ "<h1>Joystick</h1>This module helps to check if your joystick is working "
#~ "correctly.<br />If it delivers wrong values for the axes, you can try to "
#~ "solve this with the calibration.<br />This module tries to find all "
#~ "available joystick devices by checking /dev/js[0-4] and /dev/input/"
#~ "js[0-4]<br />If you have another device file, enter it in the combobox."
#~ "<br />The Buttons list shows the state of the buttons on your joystick, "
#~ "the Axes list shows the current value for all axes.<br />NOTE: the "
#~ "current Linux device driver (Kernel 2.4, 2.6) can only "
#~ "autodetect<ul><li>2-axis, 4-button joystick</li><li>3-axis, 4-button "
#~ "joystick</li><li>4-axis, 4-button joystick</li><li>Saitek Cyborg "
#~ "'digital' joysticks</li></ul>(For details you can check your Linux source/"
#~ "Documentation/input/joystick.txt)"
#~ msgstr ""
#~ "<h1>Joystick</h1>Ev module alîkarî dike ka joystick a te bi durustî "
#~ "kardike.<br />Heke ew nirxin çewt dide tewereyan,tu dikare jibo "
#~ "çareserkirna vê bi rîya Pîvan hewl bide.<br />Ev module hew ldide jibo "
#~ "dîtina hemû amûrin joystick yên berdestbar bi azmûnkirina /dev/js[0-4] û /"
#~ "dev/input/js[0-4]<br />Heke dosya amûrekê dî hebe, vêna têke combobox."
#~ "<br /> Lîsta bişkokan rewşa bişkokên  joystick a te diyar dike, lista "
#~ "tewereyan nirxa anha ya hemû tewreyan diyar dike.<br />TEBÎNÎ: ajokare "
#~ "amûra Linux yê niha (Kernel 2.4, 2.6) dikare van cureyan tenê "
#~ "bibîne<ul><li> joystickê bi 2-tewre  û 4-bişkokan </li><li>joystickê bi 3-"
#~ "tewre  û 4-bişkokan</li><li>joystickê bi 4-tewre  û 4-bişkokan</li><li> û "
#~ "Saitek Cyborg 'digital' joysticks</li></ul>(Jibo bihtirîn hûragahîyan "
#~ "temaşke li Linux source/Documentation/input/joystick.txt)"
