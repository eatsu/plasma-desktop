# Translation for plasma_shell_org.kde.plasma.desktop.po to Euskara/Basque (eu).
# Copyright (C) 2017-2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-27 01:41+0000\n"
"PO-Revision-Date: 2023-06-21 16:31+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.2\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Une honetan erabilia"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Eraman\n"
"jarduera honetara"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Erakutsi\n"
"jarduera honetan ere"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Konfiguratu"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Gelditu jarduera"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Gelditutako jarduerak:"

#: contents/activitymanager/ActivityManager.qml:120
msgid "Create activity…"
msgstr "Sortu jarduera..."

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Jarduerak"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Konfiguratu jarduera"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Ezabatu"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "Barkatu! Errore bat gertatu da %1 zamatzean."

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr "Kopiatu arbelera"

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr "Ikusi erroreen xehetasunak..."

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr "Ireki %1"

#: contents/configuration/AboutPlugin.qml:20
#: contents/configuration/AppletConfiguration.qml:244
msgid "About"
msgstr "Honi buruz"

#: contents/configuration/AboutPlugin.qml:48
msgid "Send an email to %1"
msgstr "Bidali e-posta bat %1(r)i"

#: contents/configuration/AboutPlugin.qml:62
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Ireki %1 webgunea"

#: contents/configuration/AboutPlugin.qml:130
msgid "Copyright"
msgstr "Copyright"

#: contents/configuration/AboutPlugin.qml:148 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Lizentzia:"

#: contents/configuration/AboutPlugin.qml:151
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Ikusi lizentziaren testua"

#: contents/configuration/AboutPlugin.qml:165
msgid "Authors"
msgstr "Egileak"

#: contents/configuration/AboutPlugin.qml:176
msgid "Credits"
msgstr "Merituak"

#: contents/configuration/AboutPlugin.qml:188
msgid "Translators"
msgstr "Itzultzaileak"

#: contents/configuration/AboutPlugin.qml:205
msgid "Report a Bug…"
msgstr "Akats baten berri ematea..."

#: contents/configuration/AppletConfiguration.qml:55
msgid "Keyboard Shortcuts"
msgstr "Teklatuko lasterbideak"

#: contents/configuration/AppletConfiguration.qml:292
msgid "Apply Settings"
msgstr "Ezarpenak ezarri"

#: contents/configuration/AppletConfiguration.qml:293
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Jarduneko moduluaren ezarpenak aldatu egin dira. Aldaketak ezarri edo "
"baztertu nahi dituzu?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "Ados"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "Ezarri"

#: contents/configuration/AppletConfiguration.qml:338
msgid "Cancel"
msgstr "Utzi"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Ireki konfiguratzeko orria"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Ezkerreko botoia"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Eskuineko botoia"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Erdiko botoia"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Atzera botoia"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Aurrera botoia"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Labaintze bertikala"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Labaintze horizontala"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Maius"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ktrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:98
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:170
msgctxt "@title"
msgid "About"
msgstr "Honi buruz"

#: contents/configuration/ConfigurationContainmentActions.qml:185
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Gehitu ekintza"

#: contents/configuration/ConfigurationContainmentAppearance.qml:67
msgid "Layout changes have been restricted by the system administrator"
msgstr "Antolamendu aldaketak mugatu ditu sistemako administratzaileak"

#: contents/configuration/ConfigurationContainmentAppearance.qml:82
msgid "Layout:"
msgstr "Antolamendua:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:96
msgid "Wallpaper type:"
msgstr "Horma-paper mota:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:116
msgid "Get New Plugins…"
msgstr "Lortu plugin berriak..."

#: contents/configuration/ConfigurationContainmentAppearance.qml:184
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"Antolamendu aldaketak ezarri behar dira beste aldaketak egin ahal izateko"

#: contents/configuration/ConfigurationContainmentAppearance.qml:188
msgid "Apply Now"
msgstr "Ezarri orain"

#: contents/configuration/ConfigurationShortcuts.qml:17
msgid "Shortcuts"
msgstr "Lasterbideak"

#: contents/configuration/ConfigurationShortcuts.qml:29
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""
"Lasterbide honek aplikaziotxoa aktibatuko du klikatua izan balitz bezala."

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "Horma-papera"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "Saguaren ekintzak"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Datu sarrera hemen"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:43
msgid "Panel Settings"
msgstr "Panelaren ezarpenak"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:49
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "Maximizatu"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:53
msgid "Make this panel as tall as possible"
msgstr "Panel hau ahalik altuena egitea"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:54
msgid "Make this panel as wide as possible"
msgstr "Panel hau ahalik zabalena egitea"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:62
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "Ezabatu"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:65
msgid "Remove this panel; this action is undo-able"
msgstr "Kendu panel hau; ekintza hau desegin daiteke"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:89
msgid "Alignment:"
msgstr "Lerrokatzea:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Top"
msgstr "Goian"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Left"
msgstr "Ezkerrean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr ""
"Maximizatu gabeko leiho bat goian lerrokatzen du; eraginik gabe panela "
"maximizatuta dagoenean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr ""
"Maximizatu gabeko leiho bat ezkerrera lerrokatzen du; eraginik gabe panela "
"maximizatuta dagoenean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:104
msgid "Center"
msgstr "Erdian"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:105
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr ""
"Maximizatu gabeko leiho bat erdian lerrokatzen du; eraginik gabe panela "
"maximizatuta dagoenean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Bottom"
msgstr "Behean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Right"
msgstr "Eskuinean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr ""
"Maximizatu gabeko leiho bat behean lerrokatzen du; eraginik gabe panela "
"maximizatuta dagoenean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr ""
"Maximizatu gabeko leiho bat eskuinean lerrokatzen du; eraginik gabe panela "
"maximizatuta dagoenean"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:142
msgid "Visibility:"
msgstr "Ikusgaitasuna:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:147
msgid "Always Visible"
msgstr "Beti ikusgai"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
msgid "Auto-Hide"
msgstr "Automatikoki ezkutatu"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:156
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""
"Panela ezkutuan dago, baina kurtsoreak panelaren pantailaren ertza ukitzean "
"duenean agertzen da"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:183
msgid "Opacity:"
msgstr "Opakutasuna:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:188
msgid "Always Opaque"
msgstr "Biti opakua"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:196
msgid "Adaptive"
msgstr "Moldakorra"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:197
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr ""
"Panela opakua da edozein leihok ukitzen duenean, bestela zeharrargitsua da"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:206
msgid "Always Translucent"
msgstr "Beti zeharrargitsua"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:233
msgid "Floating:"
msgstr "Higikorra:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:238
msgid "Floating"
msgstr "Higikorra"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:239
msgid "Panel visibly floats away from its screen edge"
msgstr "Panela bere pantailaren ertzetik nabarmen higitzen da"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:248
msgid "Attached"
msgstr "Atxikita"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:249
msgid "Panel is attached to its screen edge"
msgstr "Panela bere pantailaren ertzera atxikita dago"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:271
msgid "Focus Shortcut:"
msgstr "Fokuaren lasterbidea:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:281
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "Sakatu teklatuko lasterbide hau fokua Panelera eramateko"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "Arrastatu gehienezko altuera aldatzeko."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "Arrastatu gehienezko zabalera aldatzeko."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "Egin klik bikoitza berrezartzeko."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "Arrastatu gutxieneko altuera aldatzeko."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "Arrastatu gutxieneko zabalera aldatzeko."

#: contents/configuration/panelconfiguration/Ruler.qml:65
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Arrastatu pantailaren ertz horretan kokapena aldatzeko.\n"
"Egin klik bikoitza berrezartzeko."

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "Add Widgets…"
msgstr "Gehitu trepetak..."

#: contents/configuration/panelconfiguration/ToolBar.qml:27
msgid "Add Spacer"
msgstr "Gehitu bereizlea"

#: contents/configuration/panelconfiguration/ToolBar.qml:28
msgid "More Options…"
msgstr "Aukera gehiago..."

#: contents/configuration/panelconfiguration/ToolBar.qml:226
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "Arrastatu mugitzeko"

#: contents/configuration/panelconfiguration/ToolBar.qml:265
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "Erabili gezi-teklak panela mugitzeko"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel width:"
msgstr "Panelaren zabalera:"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel height:"
msgstr "Panelaren altuera:"

#: contents/configuration/panelconfiguration/ToolBar.qml:406
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Itxi"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Panelen eta mahaigainen kudeaketa"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"Panelak eta mahaigainak arrasta ditzakezu beste pantaila batzuetara "
"eramateko."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr "Trukatu %1 pantailako mahaigainarekin"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr "Eraman %1 pantailara"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
msgid "Remove Desktop"
msgstr "Kendu mahaigaina"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Kendu panela"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr "%1 (nagusia)"

#: contents/explorer/AppletAlternatives.qml:65
msgid "Alternative Widgets"
msgstr "Ordezko trepetak"

#: contents/explorer/AppletDelegate.qml:173
msgid "Undo uninstall"
msgstr "Desegin desinstalatzea"

#: contents/explorer/AppletDelegate.qml:174
msgid "Uninstall widget"
msgstr "Desinstalatu trepeta"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Egilea:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "E-posta:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Desinstalatu"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "Trepeta guztiak"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Trepetak"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "Lortu trepeta berriak..."

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Kategoriak"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets matched the search terms"
msgstr "Ez dago bilatutako terminoekin bat datorren trepetarik"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets available"
msgstr "Ez dago trepeta erabilgarririk"

#~ msgid "Switch"
#~ msgstr "Aldatu"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Above"
#~ msgstr "Leihoak atzean"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Below"
#~ msgstr "Leihoak atzean"

#, fuzzy
#~| msgid ""
#~| "Makes the panel remain visible always but part of the maximized windows "
#~| "shall go below the panel as though the panel did not exist."
#~ msgid ""
#~ "Like \"Always Visible\", but maximized and tiled windows go under the "
#~ "panel as though it didn't exist"
#~ msgstr ""
#~ "Panela beti ikusgai egotea eragiten du, maximizatutako leihoen zati bat, "
#~ "ordea, panelik ez balego bezala, paneletik behera joango da."

#~ msgid "Windows In Front"
#~ msgstr "Leihoak aurrean"

#~ msgid "Aligns the panel"
#~ msgstr "Panela lerrokatzen du"

#~ msgid "Center aligns the panel if the panel is not maximized."
#~ msgstr "Panela maximizatuta ez badago, erdi aldera lerrokatzen du."

#~ msgid ""
#~ "Makes the panel hidden always but reveals it when mouse enters the area "
#~ "where the panel would have been if it were not hidden."
#~ msgstr ""
#~ "Panela beti ezkutuan egotea eragiten du, hura erakusten du, ordea, "
#~ "ezkutuan egongo ez balitz, sagua, panela egonen litzatekeen eremuan "
#~ "sartzen denean."

#~ msgid ""
#~ "Makes the panel remain visible always but maximized windows shall cover "
#~ "it. It is revealed when mouse enters the area where the panel would have "
#~ "been if it were not covered."
#~ msgstr ""
#~ "Panela beti ikusgai egotea eragiten du, maximizatutako leihoek, ordea, "
#~ "estaliko dute. Hura erakusten du, estalita egongo ez balitz, sagua, "
#~ "panela egonen litzatekeen eremuan sartzen denean."

#~ msgid "Makes the panel translucent except when some windows touch it."
#~ msgstr ""
#~ "Panela zeharrargitsu izatea eragiten du, leihoren batek ukitzen duenean "
#~ "izan ezik."

#~ msgid "Makes the panel translucent always."
#~ msgstr "Panela beti zeharrargitsua izatea eragiten du."

#~ msgid "Makes the panel float from the edge of the screen."
#~ msgstr "Panela leihoaren ertzetik higikorra izatea eragiten du."

#~ msgid "Makes the panel remain attached to the edge of the screen."
#~ msgstr "Panela pantailaren ertzari lotuta egotea eragiten du."

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (desgaituta)"

#~ msgid "Appearance"
#~ msgstr "Itxura"

#~ msgid "Search…"
#~ msgstr "Bilatu..."

#~ msgid "Screen Edge"
#~ msgstr "Pantailaren ertza"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr ""
#~ "Egin klik eta arrastatu botoia pantailako ertz batera panela hara "
#~ "eramateko."

#~ msgid "Width"
#~ msgstr "Zabalera"

#~ msgid "Height"
#~ msgstr "Altuera"

#~ msgid "Click and drag the button to resize the panel."
#~ msgstr "Egin klik eta arrastatu botoia panelaren neurria aldatzeko."

#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "Antolamendua ezin da aldatu trepetak giltzatuta dauden bitartean"

#~ msgid "Lock Widgets"
#~ msgstr "Giltzatu trepetak"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "Lasterbide honek appleta aktibatuko du: teklatuaren fokua berari emango "
#~ "dio, eta appletak zati gainerakor bat badu (hasiera menua esaterako), "
#~ "gainerakorra irekiko da."
