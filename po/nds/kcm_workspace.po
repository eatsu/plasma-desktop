# translation of kcmworkspaceoptions.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2009, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmworkspaceoptions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-12 01:43+0000\n"
"PO-Revision-Date: 2011-08-17 11:37+0200\n"
"Last-Translator: Manfred Wiese <m.j.wiese@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ui/main.qml:24
#, kde-format
msgid ""
"The system must be restarted before changes to the middle-click paste "
"setting can take effect."
msgstr ""

#: ui/main.qml:30
#, kde-format
msgid "Restart"
msgstr ""

#: ui/main.qml:47
#, kde-format
msgid "Visual behavior:"
msgstr ""

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: ui/main.qml:48 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr ""

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: ui/main.qml:59 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr ""

#: ui/main.qml:77
#, kde-format
msgid "Animation speed:"
msgstr ""

#: ui/main.qml:103
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr ""

#: ui/main.qml:109
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr ""

#: ui/main.qml:123
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr ""

#: ui/main.qml:130
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr ""

#: ui/main.qml:144
#, kde-format
msgid "Open by double-clicking instead"
msgstr ""

#: ui/main.qml:156
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr ""

#: ui/main.qml:169
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr ""

#: ui/main.qml:184
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr ""

#: ui/main.qml:185
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr ""

#: ui/main.qml:203
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr ""

#: ui/main.qml:216
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr ""

#: ui/main.qml:229
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Middle Click:"
msgstr ""

#: ui/main.qml:231
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Pastes selected text"
msgstr ""

#: ui/main.qml:248
#, kde-format
msgid "Touch Mode:"
msgstr ""

#: ui/main.qml:250
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr ""

#: ui/main.qml:250 ui/main.qml:293
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr ""

#: ui/main.qml:266
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""

#: ui/main.qml:271
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr ""

#: ui/main.qml:311
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr ""

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr ""

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr ""

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr ""

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr ""

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr ""

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Manfred Wiese"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "m.j.wiese@web.de"

#, fuzzy
#~| msgid "Global options for the Plasma Workspace"
#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr "Globaal Optschonen för dat Plasma-Arbeitrebeet"

#~ msgid "(c) 2009 Marco Martin"
#~ msgstr "© 2009: Marco Martin"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Maintainer"
#~ msgstr "Pleger"

#, fuzzy
#~| msgid "Show Informational Tips:"
#~ msgid "Show Informational Tips"
#~ msgstr "Intressante Tipps wiesen:"
